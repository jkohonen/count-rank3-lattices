# Counting rank-3 lattices

This repository contains code and data to supplement the manuscript
"Counting graded lattices of rank three that have few coatoms"
(J. Kohonen 2018, https://arxiv.org/abs/1804.03679).

See also [OEIS: sequence A300260](https://oeis.org/A300260).

## Overview

Counting rank-3 lattices of _c_ coatoms and _a_ atoms is done in three
phases (the third phase is optional if you are content with a finite
sequence).  See the manuscript for details.

1. `gencon` List all connection graphs of _c_ coatoms.
2. `countrank3` Given the connection graphs, count the lattices up to _amax_ atoms.
3. `quasimodo` Infer a closed form expression by a quasipolynomial fit.

## Required software

* [nauty](http://pallini.di.uniroma1.it/), for listing the connection graphs (note: the GAP distribution includes a version of nauty)
* [xz](https://tukaani.org/xz/), for compressing the connection graph listing
* [GAP](https://www.gap-system.org/) and Digraphs, for computations

You may need to edit the scripts so that they find the required tools.

## Example run

This counts the rank-3 lattices that have three coatoms.

    $ ./gencon 3                # Creates connections.c3.g6.xz, containing five graphs
    $ ./countrank3 3 100        # Creates latticecounts.c3 with counts up to a=100
    $ ./quasimodo 3             # Finds closed form expression as a quasipolynomial
    ...
    Pretty-printed quasipolynomial:
      (3/4) x^{2}
    + (1/3) x
    + ([ 0, -1, -8, 3, -4, -5 ] / 12)

## Data files provided in the repository

* connections.*, connection graphs up to _c_=9 (xz-compressed graph6 format)
* latticecounts.*, numerical counts of rank3-lattices up to _c_=9 and _a_=1000 or more
* pretty.*, explicit quasipolynomials of the counts up to _c_=7

