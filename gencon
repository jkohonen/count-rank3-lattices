#!/bin/bash
#
# gencon -- Generate connection graphs for c coatoms
#
# Usage:
#    gencon c [a]
# where c is the number of coatoms, and optionally a is the number of atoms.
# If a is not specified, all possible atom counts are considered.
#
# Connection graphs are saved in xz-compressed graph6 format.
#
# Jukka Kohonen 2018

# This should point to where the nauty tool "genbg" is found.
GENBG=genbgL

let "c=$1"

if [ -z "$2" ]
then
    # All possible atom counts up to nchoosek(c,2).
    let "amax=c*(c-1)/2"
    file="connections.c${c}.g6.xz"
    for ((a=0; a<=$amax; a++))
    do
	${GENBG} -Z1 -d0:2 $c $a
    done | xz >${file}
else
    # Exactly a atoms.
    let "a=$2"
    file="connections.c${c}.a${a}.g6.xz"
    ${GENBG} -Z1 -d0:2 $c $a | xz >${file}
fi
