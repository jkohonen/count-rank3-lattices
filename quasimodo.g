# quasimodo -- fit a quasipolynomial to data file, and prettyprint

Read("qp.g");

# take data from "filename". have c boxes.
QuasimodoFromFile := function(filename,c)
    local data,xmin,xmax,xval,yval,fitval,i,d,N,xvals,yvals,qp,counteq,countne;
    
    # Read data
    Print("\nReading series from [", filename, "]\n");
    data := ReadCSV(filename, true, " ");
    
    # Perform QP fit.
    # Look at just enough terms to be able to perform the fit.
    # 
    # There are N = Lcm([1..c]) different polynomials.
    # Each polynomial has degree d = c-1, so requires c known values.
    # Also, values for x < nchoosek(c,2) may be special and cannot be
    # used in the fit.
    #
    xvals := [];
    yvals := [];
    d := c-1;
    N := Lcm([1..c]);
    xmin := c*(c-1)/2;          # nchoosek(c,2)
    xmax := xmin + N*c - 1;     # need N*c values
    for i in [1..Size(data)] do
        xval := Int(data[i].field1);
        yval := Int(data[i].field2);
        if xval >= xmin and xval <= xmax then
            Append(xvals, [xval]);
            Append(yvals, [yval]);
        fi;
    od;
    if Size(xvals) < xmax-xmin+1 then
        Error("Not enough terms. Need up to x=", xmax, "\n");
    fi;
    Print("Fitting ", d, "-degree polynomials at x=", xmin, "..", xmax, "\n");
    qp := QpFit(xvals,yvals,c-1,N);

    # Verify it with ALL known values.
    counteq := 0;
    countne := 0;
    for i in [1..Size(data)] do
        xval := Int(data[i].field1);
        yval := Int(data[i].field2);
        fitval := QpVal(qp,[xval])[1];
        if yval = fitval then
            counteq := counteq+1;
        else
            Print("Inequal at ", xval, " true=", yval, " qp=", fitval, "\n");
            countne := countne+1;
        fi;
    od;
    Print("Verify: ", counteq, " equals, ", countne, " inequals\n\n");
    
    # Prettyprint
    Print("Pretty-printed quasipolynomial:\n");
    QpPrint(qp);
    Print("\n");
    
    return qp;
end;


Quasimodo := function(c)
    local filename;
    filename := Concatenation("latticecounts.c", ViewString(c));
    return QuasimodoFromFile(filename,c);
end;

