# GroupBallsByCycleIndex
#
# Count the ways of placing 0..kmax balls into c boxes, when the boxes
# have a symmetry described by the cycle index polynomial ZG.
#
# This is based on the Cycle Index Theorem, which is Theorem 7.3 in
# Peter J. Cameron: "Notes on Counting: An Introduction to Enumerative Combinatorics".
# Cambridge University Press, 2017.
#
# See also
# https://cameroncounts.wordpress.com/2018/03/16/a-counting-problem/

    
# Elapsed real time in milliseconds
Realtime := function()
    local t;
    t := IO_gettimeofday();
    return 1000.*t.tv_sec + 0.001*t.tv_usec;
end;

GroupBallsByCycleIndex := function(ZG, c, kmax)
    local i,x,A,Axi,B,terms,rtstart;
    
    # Cycle index may use indeterminates 1..c, take next for x.
    x := Indeterminate(Rationals, c+1);
    
    rtstart := Realtime();
    
    # Figure-counting polynomial up to kmax, and substitute x^i.
    # Note that we can discard any terms higher than x^kmax.
    A := (1-x^(kmax+1)) / (1-x);
    Axi := [];
    for i in [1..c] do
        Axi[i] := Value(A, [x], [x^i]) mod x^(kmax+1);
    od;
    
    # Configuration-counting polynomial from Thm 7.3 (Cameron).
    B := Value(ZG, [1..c], Axi);
    # Print("Polya subst, elapsed ", Realtime()-rtstart, "\n");
    terms := CoefficientsOfUnivariatePolynomial(B){[1..kmax+1]};
    # Print("All done, elapsed ", Realtime()-rtstart, "\n");
    return terms;
end;

GroupBalls := function(G, c, kmax)
    return GroupBallsByCycleIndex(CycleIndex(G, [1..c]), c, kmax);
end;

