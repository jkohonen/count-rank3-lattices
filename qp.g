# QP    Functions for quasipolynomials

# Lagrange(x,y)
#
# Lagrange polynomial p such that p(x)=y exactly.

Lagrange := function(xval,yval)
    local n,j,m,x,lj,L;

    x := Indeterminate(Rationals,"x");
    L := 0;
    
    n := Size(xval);
    if Size(yval) <> n then
        return fail;
    fi;

    L := 0;
    for j in [1..n] do
        # j'th basis polynomial, with factors from m=1..k except j.
        lj := 1;
        for m in [1..n] do
            if m <> j then
                lj := lj * (x-xval[m]) / (xval[j]-xval[m]);
            fi;
        od;
        L := L + yval[j]*lj;
    od;

    return L;
end;


# QpFit
#
# Fit a quasipolynomial to the points (x,y), with degree d, and period N.
#
QpFit := function(xvals,yvals,d,N)

    local k,pos,dfit,pol,pols,needpoints;
    
    needpoints := d+1;
    
    pols := [];
    for k in [0..N-1] do
        # Fit residual class k
        pos := Positions(xvals mod N, k);
        if Size(pos) >= needpoints then
            #Print("ok\n");
        else
            Print("For k=", k, " we have ", Size(pos), " points, needing ", needpoints, " WARNING\n");
        fi;
        
        pol := Lagrange(xvals{pos}, yvals{pos});
        dfit := DegreeOfLaurentPolynomial(pol);
        if dfit > d then
            Print("Ow. We got degree ", dfit, " from ", Size(pos), " values, was expecting only ", d, "\n");
        fi;
        pols[k+1] := pol;
        
    od;
    return pols;
    
end;


# QpVal
#
# Evaluate a quasipolynomial at the points xvals
#
QpVal := function(qp,xvals)

    local x,N,k,xval,yval,yvals;
    x := Indeterminate(Rationals,"x");

    N := Size(qp);
    yvals := [];

    for xval in xvals do
        # Choose residue class
        k := xval mod N;
        # Evaluate polynomial
        yval := Value(qp[k+1], [x], [xval]);
        Append(yvals, [yval]);
    od;
    
    return yvals;
end;


# QpPrint
#
# Pretty-print a quasipolynomial, collating common terms.
#
QpPrint := function(qp)
    local N,d,dmax,k,coeff,C,T,dens,mult,i,s,folded;
    N := Size(qp);

    # Find maximum degree and gather coefficients
    dmax := 0;
    C    := [];
    for k in [0..N-1] do
        d := DegreeOfLaurentPolynomial(qp[k+1]);
        if d > dmax then
            dmax := d;
        fi;
        coeff := CoefficientsOfUnivariatePolynomial(qp[k+1]);
        C[k+1] := coeff;
    od;

    # Transpose so that degrees are the first dimension.
    T := TransposedMat(C);
          
    # Go termwise, down from maximum degree
    for d in [dmax,dmax-1..0] do
        coeff := T[d+1];
        if d = dmax then
            Print("  ");
        else
            Print("+ ");
        fi;
        
        if ForAll(coeff, c -> c=coeff[1]) then
            # All residue classes have the same coefficient.
            Print("(", coeff[1], ")");
        else
            # Different coefficients. Try to find at least a shorter period
            # and a nice common denominator.
            
            dens := List(coeff, x -> DenominatorRat(x));
            mult := Lcm(dens);

            # Try to find a period s
            for s in [2..N] do
                if N mod s = 0 then
                    folded := coeff{[0..N-1] mod s + 1};
                    if coeff = folded then
                        # Found a period!
                        break;
                    fi;
                fi;
            od;
            
            Print("(", coeff{[1..s]} * mult, " / ", mult, ")");
        fi;
        
        # Print the variable and exponent.
        if d > 1 then
            Print(" x^{", d, "}\n");
        elif d = 1 then
            # Linear term.
            Print(" x\n");
        else
            # Constant term.
            Print("\n");
        fi;
        
    od;
    
end;

