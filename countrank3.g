# countrank3.g
#
# Read rank-3 lattice connection graphs from a file.
# For each connection graph, count the ways of assigning extra atoms.

LoadPackage("digraph");
Read("groupballs.g");

showdebug := false;

Debug := function(arg...)
    if showdebug then
        CallFuncList(Print, arg);
    fi;
end;

CountRank3 := function(infilename,amax)
    local iter,G,i,ind,wrongdir,coats,conns,c,r,s,amaxremain,
          Aut,Cyc,CycSet,CycMulDic,CycMul,CycCnt,add,ways,Ways,WaysByCase,
          utime_start,rtime_start;

    utime_start := Runtime();   # in milliseconds
    rtime_start := Realtime();  # in milliseconds
    Print("\n");
    Print("CountRank3 starts\n");

    Ways := Zero([0..amax]);
    WaysByCase := [];
    
    # Lookup tables for all different cycle indices we find in the graphs.
    CycSet := Set([]);
    Cyc    := CycleIndex(SymmetricGroup(2)); # This is just an example object for the dictionary.
    CycMulDic := NewDictionary(Cyc, true);
    CycMul := [];
    CycCnt := 0;
    
    iter := IteratorFromDigraphFile(infilename);
    i := 0;
    for G in iter do
        i := i+1;
        
        # Orient the edges in atom->coatom direction only.
        # Vertices are assumed to be in order coatoms,atoms,
        # so i->j is atom->coatom iff numerically i>j.
        
        wrongdir := Filtered(DigraphEdges(G), x -> x[1]<x[2]);
        G := DigraphRemoveEdges(G, wrongdir);
        Debug("G = ", G, "\n");
        
        # Find coatoms and connector atoms.
        # Coatoms have incoming edges only (possibly none).
        # Atoms have outgoing edges only (always some).
        coats := DigraphSinks(G); # Allows isolated vertices too (just requires no outgoing)
        conns := Difference(DigraphVertices(G), coats);
        c := Size(coats);
        r := Size(conns);
        Debug("coats  ", coats, "\n");
        Debug("conns  ", conns, "\n");
        
        # Sanity checks. Check that the graph looks like we expect.
        # In particular, all coatoms before all connector atoms.
        # Connector set can be empty.
        Assert(0, IsEmpty(conns) or Maximum(coats)+1 = Minimum(conns));
        
        # Find unconnected coatoms (that do not cover any connector).
        # Each of them needs at least one "private" atom so that the lattice is graded.
        # An unconnected coatom does not have incoming edges, so it is a "source" of 
        # the digraph.
        s := Size(Intersection(coats, DigraphSources(G)));
                  
        Debug("Graph has ", r, " connectors and requires ", s, " private atoms\n");
    
        # Find the symmetries of the connection (hyper)graph.
        # We only care about the symmetries of the coatoms, not atoms.
        Aut := AutomorphismGroup(G);
        Cyc := CycleIndex(Aut, [1..c]);
        
        # Count remaining balls, when r atoms were used as connectors,
        # and s are required to make the lattice graded.
        amaxremain := amax - (r+s);

        # Find the cycle index if already known.
        AddSet(CycSet,Cyc);
        ind := LookupDictionary(CycMulDic,Cyc);
        if ind=fail then
            # New index
            CycCnt := CycCnt+1;
            ind    := CycCnt;
            AddDictionary(CycMulDic,Cyc,ind);
            CycMul[ind] := [0];
        fi;
        
        # Increase the counter for this value of r+s.
        add := [1];
        RightShiftRowVector(add, r+s, 0);
        AddCoeffs(CycMul[ind], add);
        
        if i mod 10000 = 0 then
            Print("  ... ", i, " graphs, ", Size(CycSet), " cycle indices, ",
                  "utime=", ViewString((Runtime() -utime_start)/1000.), ", ",
                  "rtime=", ViewString((Realtime()-rtime_start)/1000.), "\n");
        fi;
        
    od;
    
    Print("  Got ", i, " graphs, ", Size(CycSet), " cycle indices, ",
          "utime=", ViewString((Runtime() -utime_start)/1000.), ", ",
          "rtime=", ViewString((Realtime()-rtime_start)/1000.), "\n");
    
    i := 0;
    for Cyc in CycSet do
        i := i+1;
        ways := GroupBallsByCycleIndex(Cyc, Size(coats), amax);
        ind := LookupDictionary(CycMulDic,Cyc);
        ways := ProductCoeffs(ways, CycMul[ind]);
        ways := ways{[1..amax+1]};
        Ways := Ways+ways;
        Print("  Cycle index ", i, "/", CycCnt, ", ",
                  "utime=", ViewString((Runtime() -utime_start)/1000.), ", ",
                  "rtime=", ViewString((Realtime()-rtime_start)/1000.), "\n");
    od;
    
    Print("CountRank3 finished ",
          "utime=", ViewString((Runtime() -utime_start)/1000.), ", ",
          "rtime=", ViewString((Realtime()-rtime_start)/1000.), "\n");
    
    return Ways;
    
end;

PrintValues := function(outfilename, Data)
    local i;
    PrintTo(outfilename);
    for i in [1..Size(Data)] do
        AppendTo(outfilename, i-1, " ", Data[i], "\n");
    od;
end;

CountRank3FromFile := function(c,amax)
    local infilename,outfilename,Ways,i;
    infilename  := Concatenation("connections.c",   String(c), ".g6.xz");
    outfilename := Concatenation("latticecounts.c", String(c));
    
    Print("\nCountRank3(", c, ",", amax, ")\n");
    Print("Reading connections from [", infilename,  "]\n");
    Print("Writing counts to        [", outfilename, "]\n");
    
    Ways := CountRank3(infilename,amax);
    Print("\nsaving results...");
    PrintValues(outfilename, Ways);
    Print("done\n\n");
end;
